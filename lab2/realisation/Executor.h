#ifndef NEWLAB2_EXECUTOR_H
#define NEWLAB2_EXECUTOR_H

#include <string>
#include <vector>
#include "../interface/Worker.h"
#include "Parser.h"
#include "Validator.h"


namespace realisation {
    class Executor {
    public:
        void execute(std::string filename);
    };
}


#endif //NEWLAB2_EXECUTOR_H
