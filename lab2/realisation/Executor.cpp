#include "Executor.h"


void realisation::Executor::execute(std::string filename) {
    auto tmp = new blocks::Blockfactory();
    realisation::Parser parser;
    std::map<int, std::pair<std::string, std::vector<std::string>>> blocks = parser.parse(tmp, filename);
    std::map<int, std::pair<std::string, std::vector<std::string>>>::iterator it;

    std::vector<std::pair<std::string, std::vector<std::string>>> separated_vec;

    realisation::Validator validator;
    std::vector<int> chain = validator.getChain(filename);
    int isGood = validator.check(tmp, filename, blocks);
    if (isGood != 0)
        throw std::invalid_argument("err: {Executor}scheme is invalid");
    auto * text = new std::string("");
    int k = 0;
    for( it = blocks.begin(); it != blocks.end(); it++) {
        separated_vec.emplace_back(it->second);
        k++;
    }
    std::vector<std::string> params;
    auto i = 0;
    while ( i < chain.size()) {
        tmp->doWork(tmp->getMap(), separated_vec[i].first, text, separated_vec[i].second);
        i++;
    }
    delete tmp;
}
