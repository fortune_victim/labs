#include "Validator.h"

int realisation::Validator::check(blocks::Blockfactory *tmp, std::string filename, std::map<int, std::pair<std::string, std::vector<std::string>>> blocks) {
    int isGood = 0;
    std::vector<int> chain = this->getChain(filename);

    blockType type = tmp->getBlockType(tmp->getMap(), blocks[chain[0]].first);
    if (type != blockType::OnlyIn)
        isGood = 1;
    type = tmp->getBlockType(tmp->getMap(), blocks[chain[chain.size() - 1]].first);
    if (type != blockType::OnlyOut)
        isGood = 3;
    for (int i = 1; i < chain.size() - 1; i++) {
        type = tmp->getBlockType(tmp->getMap(), blocks[chain[i]].first);
        if (type != blockType::InOut)
            isGood = 2;
    }
    return isGood;
}

std::vector<int> realisation::Validator::getChain(std::string filename) {
    std::vector<int> chain;
    int symbIndex = 0;
    _scheme.open(filename);
    _scheme.seekg(0, std::ios::end);
    size_t size = _scheme.tellg();
    std::string instructions(size, ' ');
    _scheme.seekg(0);
    _scheme.read(&instructions[0], size);
    for (auto i = 0; i < size; i++) {
        if ((instructions[i] == '>') && (instructions[i - 1])) {
            symbIndex = i - 3;
            break;
        }
    }
    std::string number;
    while (symbIndex < size) {
        if ((instructions[symbIndex] != ' ') && (instructions[symbIndex] != '-') && (instructions[symbIndex] != '>'))
            number+=instructions[symbIndex];
        else {
            if (number.size() >= 1){
                int cnt = std::stoi(number);
                chain.push_back(cnt);
                number.clear();
            }
        }
        symbIndex++;
    }
    _scheme.close();
    return chain;
}