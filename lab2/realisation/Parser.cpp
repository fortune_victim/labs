#include "Parser.h"
#include <algorithm>
#include <vector>
std::map<int, std::pair<std::string, std::vector<std::string>>> realisation::Parser::parse(blocks::Blockfactory *tmp, std::string filename) {
    std::ifstream scheme;
    std::string str, blockName;
    std::map<int, std::pair<std::string, std::vector<std::string>>> blocks;
    int ID;
    std::vector<std::string> params;
    scheme.open(filename);
    if (!scheme.good()) {
        scheme.close();
        throw std::runtime_error("err: {Parser} we cant open input");
    }
    scheme >> str;

    std::vector<int> check;
    std::vector<int>::iterator it;

    while(1) {
        params.clear();
        scheme >> str;
        if (str == "csed")
            break;
        ID = stoi(str);
        scheme >> str;
        scheme >> str;
        blockName = str;
        int argsCount = tmp->getBlockInfo(tmp->getMap(), blockName);

        for (auto i = 1; i <= argsCount; i++){
            scheme >> str;
            params.push_back(str);
        }
        auto ait = std::find(check.begin(), check.end(), ID);
        if (ait == check.end() ) {
            blocks[ID] = make_pair(blockName, params);
            check.push_back(ID);
        }
        else throw std::runtime_error ("err: {Parser} wrong scheme. multiple call for same number of scheme");
    }
    scheme.close();
    return blocks;
}