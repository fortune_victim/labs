#ifndef NEWLAB2_PARSER_H
#define NEWLAB2_PARSER_H

#include "../interface/I_Parser.h"
#include "../blocks/writefile.h"
#include "../blocks/sort.h"
#include "../blocks/grep.h"
#include "../blocks/replace.h"
#include "../blocks/dump.h"
#include "../blocks/readfile.h"

namespace realisation {
    class Parser : public Interface::I_Parser {
    public:
        std::map <int, std::pair<std::string, std::vector<std::string>>>
        parse(blocks::Blockfactory *bf, std::string filename) override;
    };
}

#endif //NEWLAB2_PARSER_H
