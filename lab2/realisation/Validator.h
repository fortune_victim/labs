#ifndef NEWLAB2_VALIDATOR_H
#define NEWLAB2_VALIDATOR_H

#include "../interface/I_Validator.h"

namespace realisation {
    class Validator : public Interface::I_Validator {
    public:
        std::vector<int> getChain(std::string filename);
        int check(blocks::Blockfactory* bf, std::string scheme, std::map <int, std::pair<std::string, std::vector<std::string>>> blocks);
    private:
        std::ifstream _scheme;
    };
}


#endif //NEWLAB2_VALIDATOR_H
