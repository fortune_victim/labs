#include "replace.h"

void blocks::Replace::work(std::string* text, std::vector<std::string> params) {
    if (params.empty())
        throw std::runtime_error("err: {Replace} no parameters");
    if (text->empty())
        throw std::runtime_error("err: {Replace} text is empty");
    std::string res(*text);
    std::string pat = params[0], change = params[1];
    auto pos = res.find(pat);
    while (pos != std::string::npos){
        res.replace(pos, pat.size(), change);
        pos = res.find(pat, pos);
    }
    *text = res;
}

blockType blocks::Replace::getType() {
    return blockType::InOut;
}


int blocks::Replace::getArgsCount() {
    return 2;
}

Interface::Worker *blocks::Replace::create() {
    return new blocks::Replace();
}
