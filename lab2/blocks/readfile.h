#ifndef NEWLAB2_READFILE_H
#define NEWLAB2_READFILE_H

#include "../interface/Worker.h"

namespace blocks {
    class Readfile : public Interface::Worker{
    public:
        void work(std::string* text, std::vector<std::string> params);
        blockType getType();
        static Interface::Worker* create();
        int getArgsCount();
    };
}

#endif //NEWLAB2_READFILE_H
