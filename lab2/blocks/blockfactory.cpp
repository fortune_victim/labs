#include "blockfactory.h"

int blocks::Blockfactory::getBlockInfo(std::map<std::string, std::function<Interface::Worker*()>>, std::string blockName){
    std::function<Interface::Worker*()> creator = _mapOfBlocks[blockName];
    auto worker = creator();
    return worker->getArgsCount();
}

blockType blocks::Blockfactory::getBlockType(std::map<std::string, std::function<Interface::Worker*()>>, std::string blockName){
    std::function<Interface::Worker*()> creator = _mapOfBlocks[blockName];
    auto worker = creator();
    blockType a = worker->getType();
    delete worker;
    return a;
}

void blocks::Blockfactory::doWork(std::map<std::string, std::function<Interface::Worker*()>>, std::string blockName,std::string *text, std::vector<std::string> args){
    std::function<Interface::Worker*()> creator = _mapOfBlocks[blockName];
    auto worker = creator();
    worker->work(text, args);
    delete worker;
}

std::map<std::string, std::function<Interface::Worker *()>> blocks::Blockfactory::getMap() {
    return _mapOfBlocks;
}

blocks::Blockfactory::Blockfactory(){
    fillMap();
}

void blocks::Blockfactory::fillMap(){
    _mapOfBlocks["readfile"] = blocks::Readfile::create;
    _mapOfBlocks["writefile"] = blocks::Writefile::create;
    _mapOfBlocks["sort"] = blocks::Sort::create;
    _mapOfBlocks["dump"] = blocks::Dump::create;
    _mapOfBlocks["replace"] = blocks::Replace::create;
    _mapOfBlocks["grep"] = blocks::Grep::create;
}
