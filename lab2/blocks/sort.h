#ifndef NEWLAB2_SORT_H
#define NEWLAB2_SORT_H

#include "../interface/Worker.h"

namespace blocks {
    class Sort : public Interface::Worker{
    public:
        void work(std::string* text, std::vector<std::string> params);
        blockType getType();
        static Interface::Worker*create();
        int getArgsCount();
    };
}

#endif //NEWLAB2_SORT_H
