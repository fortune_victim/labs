#include "grep.h"

void blocks::Grep::work(std::string* text, std::vector<std::string> params) {
    if (params.empty())
        throw std::runtime_error("err: {Grep} no parameters");
    if (text->empty())
        throw std::runtime_error("err: {Grep} text is empty");
    std::vector<std::string> sepText;
    char symb;
    std::string str, word, grepText, pattern = params[0];
    for (int i = 0; i < text->size(); i++){
        symb = text->at(i);
        if (ispunct((int)symb)) {
            str += symb;
            continue;
        }
        if ((symb != '\n') && ((i != text->size() - 1)))
            str += symb;
        else {
            sepText.push_back(str + '\n');
            str.clear();
        }
    }
    for (int i = 0; i < sepText.size(); i++){
        if ((this->isContain(sepText[i], pattern)) == 1)
            grepText += sepText[i];
    }
    *text = grepText;
}

int blocks::Grep::isContain(std::string str, std::string example) {
    int isContain = 0;
    for (int i = 0; i < str.size(); i++){
        if (isContain == 1) break;
        if((i + example.size() - 1 > str.size()) && (isContain == 0))
            break;
        if (str[i] == example[0]){
            for (int j = 0; j < example.size(); j++){
                if (str[i+j] == example[j]){
                    if (j == example.size() - 1)
                        isContain = 1;
                }
                else break;
            }
        }
    }
    return isContain;
}

blockType blocks::Grep::getType() {
    return blockType::InOut;
}


int blocks::Grep::getArgsCount() {
    return 1;
}

Interface::Worker *blocks::Grep::create() {
    return new blocks::Grep();
}
