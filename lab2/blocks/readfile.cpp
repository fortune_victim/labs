#include "readfile.h"
#include <iostream>

void blocks::Readfile::work(std::string* text, std::vector<std::string> params) {
    std::string fileName = params[0];
    if (params.empty())
        throw std::runtime_error("err: {Readfile} no parameters");
    std::ifstream input;
    input.exceptions(std::ifstream::badbit);
    input.open(fileName);
    if (!input.good()) {
        input.close();
        throw std::runtime_error("err: {Readfile} we cant open input_file");
    }
    std::string str;
    std::vector<std::string> vec;
    int sentCount = 0;
    str.clear();
    while (getline(input, str)){
        sentCount++;
        vec.push_back(str);
        *text += str + '\n';
    }
}

blockType blocks::Readfile::getType() {
    return blockType::OnlyIn;
}

int blocks::Readfile::getArgsCount() {
    return 1;
}

Interface::Worker *blocks::Readfile::create() {
    return new blocks::Readfile();
}
