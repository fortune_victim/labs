#ifndef NEWLAB2_BLOCKFACTORY_H
#define NEWLAB2_BLOCKFACTORY_H

#include "../interface/Worker.h"
#include "../blocks/writefile.h"
#include "../blocks/sort.h"
#include "../blocks/grep.h"
#include "../blocks/replace.h"
#include "../blocks/dump.h"
#include "../blocks/readfile.h"
#include <functional>

namespace blocks {
    class Blockfactory {
    public:
        Blockfactory();
        int getBlockInfo(std::map<std::string, std::function<Interface::Worker*()>>, std::string blockName);
        blockType getBlockType(std::map<std::string, std::function<Interface::Worker*()>>, std::string blockName);
        void doWork(std::map<std::string, std::function<Interface::Worker*()>>, std::string blockName, std::string *text, std::vector<std::string> args);
        std::map<std::string, std::function<Interface::Worker*()>> getMap();
        void fillMap();
    private:
        std::map<std::string, std::function<Interface::Worker*()>> _mapOfBlocks;
    };
}


#endif //NEWLAB2_BLOCKFACTORY_H
