#ifndef NEWLAB2_GREP_H
#define NEWLAB2_GREP_H

#include "../interface/Worker.h"

namespace blocks {
    class Grep : public Interface::Worker {
    public:
        void work(std::string* text, std::vector<std::string> params);
        blockType getType();
        static Interface::Worker* create();
        int getArgsCount();
    private:
        int isContain (std::string str, std::string example);
    };
}

#endif //NEWLAB2_GREP_H
