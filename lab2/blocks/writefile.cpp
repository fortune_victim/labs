#include "writefile.h"

void blocks::Writefile::work(std::string* text, std::vector<std::string> params) {
    std::string fileName = params[0];
    std::string test;
    for (auto &k : *text){
        test += k;
    }
    if (params.empty())
        throw std::runtime_error("err: {Writefile} no parameters");
    if (text->empty())
        throw std::runtime_error("err:{Writefile} text is empty");
    std::ofstream out;
    out.exceptions(std::ofstream::badbit);
    out.open(fileName);
    if (!out.good()) {
        out.close();
        throw std::runtime_error("err: {Writefile} we cant open out_file");
    }
    out << *text;
    out.close();
}

blockType blocks::Writefile::getType() {
    return blockType::OnlyOut;
}

int blocks::Writefile::getArgsCount() {
    return 1;
}

Interface::Worker * blocks::Writefile::create() {
    return new Writefile();
}