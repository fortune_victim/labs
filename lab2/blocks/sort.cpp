#include "sort.h"

void blocks::Sort::work(std::string* text, std::vector<std::string> params) {
    std::string str;
    char symb, k = 0;
    std::multimap <std::string, int> Map ;
    std::multimap <std::string, int>::iterator();
    for(int i = 0; i < text->size(); i++){
        symb = text->at(i);
        if ((symb != '\n') && (i != text->size()-1))
            str += symb;
        else{
            Map.insert(std::pair<std::string, int>(str, k));
            k++;
            str.clear();
        }
    }
    std::string sortedText;
    for (auto &it : Map){
        sortedText += it.first + '\n';
    }
    *text = sortedText;
}

blockType blocks::Sort::getType() {
    return blockType::InOut;
}


int blocks::Sort::getArgsCount() {
    return 0;
}

Interface::Worker* blocks::Sort::create(){
    return new blocks::Sort();
};

