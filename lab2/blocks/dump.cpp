#include "dump.h"

void blocks::Dump::work(std::string* text, std::vector<std::string> params) {
    std::string fileName = params[0];
    if (params.empty())
        throw std::runtime_error("err: {Dump} no parameters");
    if (text->empty())
        throw std::runtime_error("err: {Dump} text is empty");
    std::ofstream out;
    out.exceptions(std::ofstream::badbit);
    out.open(fileName, std::ofstream::trunc);
    if (!out.good()) {
        out.close();
        throw std::runtime_error("err: {Dump} we cant open out_file");
    }
    out << *text;
    out.close();
}

blockType blocks::Dump::getType() {
    return blockType::InOut;
}

int blocks::Dump::getArgsCount() {
    return 1;
}

Interface::Worker *blocks::Dump::create() {
    return new blocks::Dump();
}
