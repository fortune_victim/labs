#ifndef NEWLAB2_I_VALIDATOR_H
#define NEWLAB2_I_VALIDATOR_H

#include <map>
#include <string>
#include <vector>
#include "../blocks/blockfactory.h"

namespace Interface {
    class I_Validator {
    public:
        I_Validator(){};
        virtual ~I_Validator() {};
        virtual std::vector<int> getChain(std::string filename) = 0;
        virtual int check(blocks::Blockfactory* bf, std::string scheme,
                          std::map <int, std::pair<std::string, std::vector<std::string>>> blocks) = 0;
    };
}

#endif //NEWLAB2_I_VALIDATOR_H
