#ifndef NEWLAB2_WORKER_H
#define NEWLAB2_WORKER_H

#include <map>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>

enum class blockType { OnlyIn, OnlyOut, InOut};

namespace Interface {
    class Worker {
    public:
        virtual void work(std::string *text, std::vector<std::string> args) = 0;
        virtual blockType getType() = 0;
        virtual int getArgsCount() = 0;
        virtual ~Worker() {};
    };
}

#endif //NEWLAB2_WORKER_H
