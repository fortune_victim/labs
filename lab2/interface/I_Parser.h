#ifndef NEWLAB2_I_PARSER_H
#define NEWLAB2_I_PARSER_H

#include <map>
#include <string>
#include <vector>
#include "../blocks/blockfactory.h"

namespace Interface {
    class I_Parser {
    public:
        I_Parser(){};
        virtual ~I_Parser() {};
        virtual std::map<int, std::pair<std::string, std::vector<std::string>>>
        parse(blocks::Blockfactory *bf, std::string filename) = 0;
    };
}


#endif //NEWLAB2_I_PARSER_H
