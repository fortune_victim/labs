#include <iostream>
#include "realisation/Executor.h"

int main(int argc, char* argv[]) {
    std::string schemeName;
    schemeName = argv[1];
    std::string sheme(schemeName);
    realisation::Executor executor;
    executor.execute(sheme);
    return 0;
}