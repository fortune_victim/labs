#include <iostream>
#include "CsvParser.h"

int main() {
    std::cout << "Enter name of file inf format (read.csv), then delimiter(;)[one symbol] and then how much lines you want to skip(2)[also if skip is NaN it will be set as 0]: \n";
    std::string tmp, inp, input_file_name;
    int skip;
    char delim;
    std::vector<std::string> vecIn;
    getline(std::cin, inp);
    std::istringstream ss(inp);
    while (ss >> tmp)
        vecIn.push_back(tmp);
    input_file_name = vecIn[0];
    skip = atoi(vecIn[2].c_str());
    tmp = vecIn[1];
    if (!tmp[1])
        delim = tmp[0];
    else
        throw std::runtime_error("delimiter is too big");
    try {
        CsvParser<float, int, int, std::string> parser(input_file_name, delim, skip);
        for(auto rs : parser){
            std::cout << rs << std::endl;
        }
    } catch (std::invalid_argument &err) {
        std::cerr << err.what() << std::endl;
    } catch (ParserException &exception) {
        std::cerr << exception.what() << std::endl;
        return 0;
    } catch (TypeMismatch &err) {
        std::cerr << err.what() << " Row: " << err.get_row() << " Column: " << err.get_column() << std::endl;
        return 0;
    }

    return 0;
}