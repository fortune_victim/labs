#ifndef LAB4_PARSEERROR_H
#define LAB4_PARSEERROR_H

#include <string>
#include <utility>

class ParserException {
private:
    std::string m_error;
public:
    explicit ParserException (std::string error) : m_error(std::move(error)) {}
    const char* what() { return m_error.c_str();}
};

class TypeMismatch {
private:
    std::string m_error;
    int curr_column;
    int curr_row;
public:
    TypeMismatch(std::string error, int col, int row) :
            m_error(std::move(error)), curr_column(col), curr_row(row) {}
    const char* what(){ return m_error.c_str();}
    int get_column(){ return (curr_column + 1);}
    int get_row(){ return (curr_row + 1);}
};

#endif //LAB4_PARSEERROR_H