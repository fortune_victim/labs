cmake_minimum_required(VERSION 3.5)
project(Upper_project)

find_package(GTest REQUIRED)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=gnu++11")

add_subdirectory(lib/googletest-master)
include_directories(lib/googletest-master/googletest/include)
include_directories(lib/googletest-master/googlemock/include)

include_directories(${GTEST_INCLUDE_DIRS})

set(SOURCE_FILES main.cpp tests/TestForSet.cpp)

add_executable(gtestsetexe ${SOURCE_FILES})

target_link_libraries(gtestsetexe lab1_src gtest gtest_main ${GTEST_LIBRARIES} pthread)
