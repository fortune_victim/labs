#ifndef BATTLESHIP_GAMEPREFERENCES_H
#define BATTLESHIP_GAMEPREFERENCES_H

#include <iostream>
#include <map>
#include <functional>
#include <vector>

const size_t COLUMNS = 10;
const size_t ROWS = 10;
const size_t EDGELEN = 10;
const size_t NUMOFDIRS = 4;
const size_t DOUBLEROWS = 21;
const size_t LOWERBOUND = 0;
const size_t STARTPOINT = 0;
const size_t UPPERBOUND = 9;
const size_t SOMECOORD = 6;

const size_t ROTATE = 1;
const size_t START = 0;
const size_t LEFT = 0;
const size_t RIGHT = 1;
const size_t BOT = 2;
const size_t TOP = 3;

const size_t SMARTLOWERBOUND = 1;
const size_t SMARTUPPERBOUD = 8;
const size_t SMARTTOLEFT = 2;
const size_t SMARTTORIGHT = 7;

const size_t NUMBEROFDIMS = 2;
const size_t NOTPUT = 0;
const size_t PUT = 1;
const size_t NOWINS = 0;
const size_t NOTFILLED = 0;
const size_t OVERALLTYPES = 4;
const size_t BATTLEBOAT = 1;
const size_t CRUISERS = 2;
const size_t DESTROYERS = 3;
const size_t TORPEDOBOATS = 4;
const size_t TBCOUNT = 4;

const size_t BBLENABS = 4;
const size_t CRLENABS = 3;
const size_t DSLENABS = 2;
const size_t TBLENABS = 1;

const size_t BBLEN = 3;
const size_t CRLEN = 2;
const size_t DSLEN = 1;
const size_t TBLEN = 0;

const size_t TOPLEFT = 0;
const size_t TOPRIGHT = 1;
const size_t BOTRIGHT = 2;
const size_t BOTLEFT = 3;

const size_t NUMBER_OF_CELLS_W_SHIPS = 20;
const size_t ONPROBRETURN = 25;
const size_t ONPROBDISCARD = 0;

const char CLEAR_CELL = '~';
const char MISS_CELL = '.';
const char HIT_CELL = 'o';
const char SPACE = ' ';
const std::string TRIPLESPACE = "   ";

#endif //BATTLESHIP_GAMEPREFERENCES_H
