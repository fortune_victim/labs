#include <ctime>
#include "GameController.h"

GameController::GameController(){
    _mapOfGamers[CONSOLE] = ConsoleGamer::create;
    _mapOfGamers[RANDOM] = RandomGamer::create;
    _mapOfGamers[SMART] = SmartGamer::create;
}

void GameController::initMaps(){
    gamer1_map.clear();
    gamer2_map.clear();
    gamer1_attack_map.clear();
    gamer2_attack_map.clear();
}

bool GameController::setNcheck(Map &gamerAMap, Map &gamerB_attackMap, std::pair<size_t, size_t> &gameStep,
                                size_t &hitCells ) {
    if (gamerAMap.getCell(gameStep.first, gameStep.second) == SHIP) {
        ++hitCells;
        gamerB_attackMap.setCell(gameStep.first, gameStep.second, HIT);
        gamerAMap.setCell(gameStep.first, gameStep.second, EMPTY);
    } else {
        gamerB_attackMap.setCell(gameStep.first, gameStep.second, MISS);
        return false;
    }
    return true;
}

std::string GameController::printRes(std::pair<size_t, size_t> &gameScore){
    std::string str;
    if(gameScore.first > gameScore.second)
        return("Gamer 1 won the game!!");
    else if(gameScore.first < gameScore.second)
        return("Gamer 2 won the game!!");
    else if (gameScore.first == gameScore.second)
        return("Draw happened!!!");
    else return ("Unexpected result");
}

void GameController::run(const GamerMode &gamerMode_1, const GamerMode &gamerMode_2, const size_t &countGames) {
    srand(time(0));
    GameMode mode;
    ConsoleView view;
    std::pair<size_t, size_t> gameStep;
    std::pair<size_t, size_t> hitCells;
    std::pair<size_t, size_t> gameScore = std::make_pair(0, 0);

    std::function<IGamer*()> creator = _mapOfGamers[gamerMode_1];
    auto gamer_1 = creator();
    creator = _mapOfGamers[gamerMode_2];
    auto gamer_2 = creator();

    for (size_t i = START; i < countGames; i++) {
        mode = IN_PROCESS;
        initMaps();
        gamer_1->fillMap(gamer1_map);
        gamer_2->fillMap(gamer2_map);
        hitCells = std::make_pair(NOWINS, NOWINS);
        view.drawGameState(gamer1_attack_map, gamer2_attack_map, gameScore);
        while (mode == IN_PROCESS) {
            while (true) {
                view.drawGameState(gamer1_attack_map, gamer2_attack_map, gameScore);
                view.printMessage("Gamer 1, make a step");
                gameStep = gamer_1->attack(gamer1_attack_map);
                if (!setNcheck(gamer2_map, gamer1_attack_map, gameStep, hitCells.first))
                    break;
                if (hitCells.first == NUMBER_OF_CELLS_W_SHIPS) {
                    mode = WIN_GAMER_1;
                    ++gameScore.first;
                    break;
                }
            }
            if (mode != WIN_GAMER_1) {
                while (true) {
                    view.drawGameState(gamer1_attack_map, gamer2_attack_map, gameScore);
                    view.printMessage("Gamer 2, make a step");
                    gameStep = gamer_2->attack(gamer2_attack_map);
                    if (!setNcheck(gamer1_map, gamer2_attack_map, gameStep, hitCells.second))
                        break;
                    if (hitCells.second == NUMBER_OF_CELLS_W_SHIPS) {
                        mode = WIN_GAMER_2;
                        ++gameScore.second;
                        break;
                    }
                }
            }
        }

        view.drawGameState(gamer1_attack_map, gamer2_attack_map, gameScore);
        std::string mess = "Gamer " + std::to_string(mode) + " won the lap";
        view.printMessage(mess);
        std::cout << "\n Press 'Enter' to continue' \n";
        std::cin.get();
    }
    view.printMessage(printRes(gameScore));
}