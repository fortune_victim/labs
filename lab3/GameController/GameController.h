#ifndef BATTLESHIP_GAMECONTROLLER_H
#define BATTLESHIP_GAMECONTROLLER_H

#include "../Interfaces/IGameController.h"
#include "../ConsoleView/ConsoleView.h"
#include "../Gamers/RandomGamer.h"
#include "../Gamers/ConsoleGamer.h"
#include "../Gamers/SmartGamer.h"

enum GameMode {
    IN_PROCESS, WIN_GAMER_1, WIN_GAMER_2
};

class GameController : public IGameController {
public:
    GameController();
    void run(const GamerMode &gamerMode_1, const GamerMode &gamerMode_2, const size_t &countGames) override;
private:
    std::map<GamerMode, std::function<IGamer*()>> _mapOfGamers;
    Map gamer1_map, gamer1_attack_map, gamer2_map, gamer2_attack_map;
    void initMaps();

    static bool setNcheck(Map &gamerAMap, Map &gamerB_attackMap, std::pair<size_t, size_t> &gameStep,
                   size_t &hitCells);

    static std::string printRes(std::pair<size_t, size_t> &gameScore);
};

#endif //BATTLESHIP_GAMECONTROLLER_H
