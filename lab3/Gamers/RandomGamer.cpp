#include <random>
#include "RandomGamer.h"

IGamer * RandomGamer::create(){
    return new RandomGamer();
}

bool canPutShip(int x0, int y0, int x1, int y1, Map map) {
    if (x0 < LOWERBOUND || y0 < LOWERBOUND || x1 < LOWERBOUND || y1 < LOWERBOUND ||
        x0 > UPPERBOUND || y0 > UPPERBOUND || x1 > UPPERBOUND || y1 > UPPERBOUND)
        return false;
    for (int i = std::min(x0, x1) - 1; i <= std::max(x0, x1) + 1; i++) {
        for (int j = std::min(y0, y1) - 1; j <= std::max(y0, y1) + 1; j++) {
            if (i >= STARTPOINT && i < COLUMNS &&
                j >= STARTPOINT && j < ROWS &&
                map.getCell(i, j) != EMPTY)
                return false;
        }
    }
    return true;
}

void RandomShip(int amountShips, int longShip, Map &map){
    int dir;
    int x0, y0, x1, y1;
    for (int i = START; i < amountShips; ++i) {
        while(true){
            x0 = rand() % COLUMNS;
            y0 = rand() % ROWS;
            dir = rand() % NUMOFDIRS;
            if (map.getCell(x0, y0) == EMPTY) {
                if (dir == LEFT && canPutShip(x0, y0, x0, y0 - longShip, map)) {
                    y1 = y0 - longShip;
                    x1 = x0;
                    break;
                }
                if (dir == RIGHT && canPutShip(x0, y0, x0, y0 + longShip, map)) {
                    x1 = x0;
                    y1 = y0 + longShip;
                    break;
                }
                if (dir == BOT && canPutShip(x0, y0, x0 - longShip, y0, map)) {
                    y1 = y0;
                    x1 = x0 - longShip;
                    break;
                }
                if (dir == TOP && canPutShip(x0, y0, x0 + longShip, y0, map)) {
                    y1 = y0;
                    x1 = x0 + longShip;
                    break;
                }
            }
        }
        for (int j = START; j < longShip + 1; ++j) {
            if (x0 == x1) {
                int direction = y1 - y0;
                map.setCell(x0, direction > 0 ? y0 + j : y0 - j, SHIP);
            }
            if (y0 == y1) {
                int direction = x1 - x0;
                map.setCell(direction > 0 ? x0 + j : x0 - j, y0, SHIP);
            }
        }
    }
}

std::pair<size_t, size_t> RandomGamer::attack(const Map &attackMap) {
    size_t x = rand() % COLUMNS, y = rand() % ROWS;
    while (attackMap.getCell(x, y) != EMPTY) {
        x = rand() % COLUMNS;
        y = rand() % ROWS;
    }
    return std::make_pair(x, y);
}

void RandomGamer::fillMap(Map &map) {
    RandomShip(BATTLEBOAT, BBLEN, map);
    RandomShip(CRUISERS, CRLEN, map);
    RandomShip(DESTROYERS, DSLEN, map);
    RandomShip(TORPEDOBOATS, TBLEN, map);
}