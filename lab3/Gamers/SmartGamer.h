#ifndef BATTLESHIP_SMARTGAMER_H
#define BATTLESHIP_SMARTGAMER_H

#include "../Interfaces/IGamer.h"

class SmartGamer : public IGamer {
public:

    std::pair<size_t, size_t> attack(const Map &attackMap) override;
    void fillMap(Map &map) override;
    static IGamer *create();

private:
    std::map <int, std::pair<int, int>> _mapOfCords0;
    std::map <int, std::pair<int, int>> _mapOfCords1;
    void initMap();
    void putAngle(Map &map, int *angleShips, int longShip);

    void setMapAngle(int longShip, int dir);
    void putSides(Map &map);
    bool putSidesCondition(int &x0, int &y0, int &x1, int &y1, Map &map, int &count);
    std::pair<size_t, size_t> xnext(size_t &x, size_t &y, const Map &attackMap);
    std::pair<size_t, size_t> ynextIn(size_t &x, size_t &y, const Map &attackMap, size_t &step);
    std::pair<size_t, size_t> ynext(size_t &x, size_t &y, const Map &attackMap, size_t &step);
};


#endif //BATTLESHIP_SMARTGAMER_H
