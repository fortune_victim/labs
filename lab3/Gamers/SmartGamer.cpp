#include "SmartGamer.h"


IGamer * SmartGamer::create(){
    return new SmartGamer();
}

bool canPutShip1(int x0, int y0, int x1, int y1, Map map) {
    if (x0 < LOWERBOUND || y0 < LOWERBOUND || x1 < LOWERBOUND || y1 < LOWERBOUND ||
        x0 > UPPERBOUND || y0 > UPPERBOUND || x1 > UPPERBOUND || y1 > UPPERBOUND)
        return false;
    for (int i = std::min(x0, x1) - 1; i <= std::max(x0, x1) + 1; i++) {
        for (int j = std::min(y0, y1) - 1; j <= std::max(y0, y1) + 1; j++) {
            if (i >= STARTPOINT && i < COLUMNS &&
                j >= STARTPOINT && j < ROWS &&
                map.getCell(i, j) != EMPTY)
                return false;
        }
    }
    return true;
}

void SmartGamer::initMap() {
    _mapOfCords0[TOPLEFT] = std::make_pair(LOWERBOUND,LOWERBOUND);
    _mapOfCords0[TOPRIGHT] = std::make_pair(UPPERBOUND,LOWERBOUND);
    _mapOfCords0[BOTRIGHT] = std::make_pair(UPPERBOUND,UPPERBOUND);
    _mapOfCords0[BOTLEFT] = std::make_pair(LOWERBOUND,UPPERBOUND);

    _mapOfCords1[TOPLEFT] = std::make_pair(LOWERBOUND,LOWERBOUND);
    _mapOfCords1[TOPRIGHT] = std::make_pair(LOWERBOUND,LOWERBOUND);
    _mapOfCords1[BOTRIGHT] = std::make_pair(LOWERBOUND,LOWERBOUND);
    _mapOfCords1[BOTLEFT] = std::make_pair(LOWERBOUND,LOWERBOUND);
}

void SmartGamer::setMapAngle(int longShip, int dir){
    if (dir == ROTATE){
        _mapOfCords1[TOPLEFT] = std::make_pair(_mapOfCords0[TOPLEFT].first + longShip,LOWERBOUND);
        _mapOfCords1[TOPRIGHT] = std::make_pair(_mapOfCords0[TOPRIGHT].first - longShip, LOWERBOUND);
        _mapOfCords1[BOTRIGHT] = std::make_pair(_mapOfCords0[BOTRIGHT].first - longShip, UPPERBOUND);
        _mapOfCords1[BOTLEFT] = std::make_pair(_mapOfCords0[BOTLEFT].first + longShip,UPPERBOUND);
    }
    else {
        _mapOfCords1[TOPLEFT] = std::make_pair(LOWERBOUND,_mapOfCords0[TOPLEFT].second + longShip);
        _mapOfCords1[TOPRIGHT] = std::make_pair(UPPERBOUND, _mapOfCords0[TOPRIGHT].second + longShip);
        _mapOfCords1[BOTRIGHT] = std::make_pair(UPPERBOUND, SOMECOORD);
        _mapOfCords1[BOTLEFT] = std::make_pair(LOWERBOUND, _mapOfCords0[BOTLEFT].second - longShip);
    }
}

void SmartGamer::putAngle(Map &map, int *angleShips, int longShip) {
    int angle;
    int dir = (rand() % 2) + 1;
    while (true) {
        angle = rand() % NUMOFDIRS;
        if (angleShips[angle] == NOTPUT) break;
    }
    angleShips[angle] = PUT;
    initMap();
    setMapAngle(longShip, dir);

    for (int j = START; j < longShip + 1; ++j) {
        if (_mapOfCords0[angle].first == _mapOfCords1[angle].first) {
            int direction = _mapOfCords1[angle].second - _mapOfCords0[angle].second;
            map.setCell(_mapOfCords0[angle].first,
                        direction > 0 ? _mapOfCords0[angle].second + j : _mapOfCords0[angle].second - j, SHIP);
        }
        if (_mapOfCords0[angle].second == _mapOfCords1[angle].second) {
            int direction = _mapOfCords1[angle].first - _mapOfCords0[angle].first;
            map.setCell(direction > 0 ? _mapOfCords0[angle].first + j : _mapOfCords0[angle].first - j, _mapOfCords0[angle].second, SHIP);
        }
    }

}

void SmartGamer::putSides(Map &map) {
    int x0, y0, x1, y1;
    int count = LOWERBOUND;

    x0 = x1 = LOWERBOUND;
    for (y0 = LOWERBOUND; y0 < UPPERBOUND; y0++) {
        y1 = y0 + 1;
        bool check = putSidesCondition(x0,y0,x1,y1,map,count);
        if (!check) break;
    }

    y0 = y1 = UPPERBOUND;
    for (x0 = LOWERBOUND; x0 < UPPERBOUND; x0++) {
        x1 = x0 + 1;
        bool check = putSidesCondition(x0,y0,x1,y1,map,count);
        if (!check) break;
    }

    if (count < 2) {
        x0 = x1 = UPPERBOUND;
        for (y0 = UPPERBOUND; y0 > LOWERBOUND; y0--) {
            y1 = y0 - 1;
            bool check = putSidesCondition(x0,y0,x1,y1,map,count);
            if (!check) break;
        }
    }

    if (count < 2) {
        y0 = y1 = LOWERBOUND;
        for (x0 = UPPERBOUND; x0 > LOWERBOUND; x0--) {
            x1 = x0 - 1;
            bool check = putSidesCondition(x0,y0,x1,y1,map,count);
            count--;
            if (!check) break;
        }
    }
}

bool SmartGamer::putSidesCondition(int &x0, int &y0, int &x1, int &y1, Map &map, int &count) {
    if (canPutShip1(x0, y0, x1, y1, map)) {
        for (int j = START; j < NUMBEROFDIMS; ++j) {
            if (x0 == x1) {
                int direction = y1 - y0;
                map.setCell(x0, direction > 0 ? y0 + j : y0 - j, SHIP);
            }
            if (y0 == y1) {
                int direction = x1 - x0;
                map.setCell(direction > 0 ? x0 + j : x0 - j, y0, SHIP);
            }
        }
        count++;
        return false;
    }
    return true;
}

void putSingles(Map &map) {
    int x0, y0;
    for (int i = START; i < TBCOUNT; i++) {
        while (true) {
            x0 = rand() % COLUMNS;
            y0 = rand() % ROWS;
            if (map.getCell(x0, y0) == EMPTY && canPutShip1(x0, y0, x0, y0, map)) break;
        }
        map.setCell(x0, y0, SHIP);
    }
}
std::pair<size_t,size_t> SmartGamer::xnext(size_t &x, size_t &y, const Map &attackMap){
    if (((x > SMARTLOWERBOUND) && attackMap.getCell(x - 1, y) == HIT) ||
        ((x < SMARTUPPERBOUD) && attackMap.getCell(x + 1, y) == HIT)) {
        if ((x > SMARTTOLEFT) && attackMap.getCell(x - 2, y) == EMPTY) {
            return std::make_pair(x - 2, y);
        }
        if ((x < SMARTTORIGHT) && attackMap.getCell(x + 2, y) == EMPTY) {
            return std::make_pair(x + 2, y);
        }
        return std::make_pair(ONPROBRETURN,0);
    }
    return std::make_pair(ONPROBDISCARD,0);
}

std::pair<size_t, size_t> SmartGamer::ynext(size_t &x, size_t &y, const Map &attackMap, size_t &step){
    std::pair<size_t, size_t> tmp;
    if (((y > SMARTLOWERBOUND) && attackMap.getCell(x, y - 1) == HIT) || ((y < SMARTUPPERBOUD) && attackMap.getCell(x, y + 1) == HIT)) {
        if ((y > SMARTTOLEFT) && attackMap.getCell(x, y - 2) == EMPTY) {
            return std::make_pair(x, y - 2);
        }
        if ((y < SMARTTORIGHT) && attackMap.getCell(x, y + 2) == EMPTY) {
            return std::make_pair(x, y + 2);
        } else {
            while (true) {
                step = rand() % (EDGELEN - x);
                for (size_t i = START; i < step; i++) {
                    step -= i;
                    tmp = ynextIn(x,y,attackMap,step);
                    if (tmp.first != ONPROBDISCARD)
                        return tmp;
                }
                break;
            }
        }
    }
    return std::make_pair(ONPROBDISCARD, 0);
}

std::pair<size_t, size_t> SmartGamer::ynextIn(size_t &x, size_t &y, const Map &attackMap, size_t &step){
    if (x < UPPERBOUND - step && attackMap.getCell(x + step, y) == EMPTY)
        return std::make_pair(x + step, y);

    if (y < UPPERBOUND - step && attackMap.getCell(x, y + step) == EMPTY)
        return std::make_pair(x, y + step);

    if (x > step && attackMap.getCell(x - step, y) == EMPTY)
        return std::make_pair(x - step, y);

    if (y > step && attackMap.getCell(x, y + step) == EMPTY)
        return std::make_pair(x, y - step);

    if (x < UPPERBOUND - step && y < UPPERBOUND - step && attackMap.getCell(x + step, y + step) == EMPTY)
        return std::make_pair(x + step, y + step);

    if (y < UPPERBOUND - step && x > step && attackMap.getCell(x, y + step) == EMPTY)
        return std::make_pair(x - step, y + step);

    if (x > step && y > step && attackMap.getCell(x - step, y) == EMPTY)
        return std::make_pair(x - step, y - step);

    if (y > step && x < UPPERBOUND - step && attackMap.getCell(x, y + step) == EMPTY)
        return std::make_pair(x + step, y - step);
    return std::make_pair (ONPROBDISCARD, 0);
}

std::pair<size_t, size_t> SmartGamer::attack(const Map &attackMap) {
    size_t x, y, step;
    while (true) {
        x = rand() % COLUMNS;
        y = rand() % ROWS;
        if (attackMap.getCell(x, y) == EMPTY) {
            return std::make_pair(x, y);
        }
        if (attackMap.getCell(x, y) == HIT) {
            auto tmp = xnext(x,y,attackMap);
            if (tmp.first != ONPROBRETURN)
                return tmp;
            else if (tmp.first == ONPROBRETURN)
                continue;
            tmp = ynext(x,y,attackMap, step);
            if (tmp.first != ONPROBDISCARD)
                return tmp;
        }
    }
}

void SmartGamer::fillMap(Map &map) {
    int angleShips[OVERALLTYPES] = {NOTFILLED};
    putAngle(map, angleShips, BBLEN);
    putAngle(map, angleShips, CRLEN);
    putAngle(map, angleShips, CRLEN);
    putAngle(map, angleShips, DSLEN);

    putSides(map);
    putSingles(map);
}
