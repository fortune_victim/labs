#ifndef BATTLESHIP_RANDOMGAMER_H
#define BATTLESHIP_RANDOMGAMER_H

#include "../Interfaces/IGamer.h"

class RandomGamer : public IGamer {
public:
    std::pair<size_t, size_t> attack(const Map &attackMap) override;
    void fillMap(Map &map) override;
    static IGamer *create();
};

#endif //BATTLESHIP_RANDOMGAMER_H
