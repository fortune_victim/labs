#ifndef BATTLESHIP_CONSOLEGAMER_H
#define BATTLESHIP_CONSOLEGAMER_H

#include "../Interfaces/IGamer.h"

class ConsoleGamer : public IGamer {
public:
    std::pair<size_t, size_t> attack(const Map &attackMap) override;
    void fillMap(Map &map) override;
    static IGamer *create();
};

#endif //BATTLESHIP_CONSOLEGAMER_H
