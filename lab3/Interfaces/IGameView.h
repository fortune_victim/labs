#ifndef BATTLESHIP_IGAMEVIEW_H
#define BATTLESHIP_IGAMEVIEW_H

#include "..//Map//Map.h"
#include "../GameController/GameController.h"

class IGameView {
public:
    virtual void drawMap(const Map&) = 0;
    virtual void drawGameState(const Map&, const Map&, const std::pair<size_t, size_t>&) = 0;
    virtual void printMessage(const std::string&) = 0;
};

#endif //BATTLESHIP_IGAMEVIEW_H
