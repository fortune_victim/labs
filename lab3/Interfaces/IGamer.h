#ifndef BATTLESHIP_IGAMER_H
#define BATTLESHIP_IGAMER_H

#include "../Map/Map.h"

enum GamerMode{CONSOLE, RANDOM, SMART};

class IGamer {
public:
    virtual std::pair<size_t, size_t> attack(const Map &attackMap) = 0;
    virtual void fillMap(Map &map) = 0;
    virtual ~IGamer(){};
};

#endif //BATTLESHIP_IGAMER_H
