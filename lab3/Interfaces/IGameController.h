#ifndef BATTLESHIP_IGAMECONTROLLER_H
#define BATTLESHIP_IGAMECONTROLLER_H

#include "../Interfaces/IGamer.h"

class IGameController {
    virtual void run(const GamerMode &gamerMode_1, const GamerMode &gamerMode_2, const size_t &countGames) = 0;
};

#endif //BATTLESHIP_IGAMECONTROLLER_H
