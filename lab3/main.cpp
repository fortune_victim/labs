#include <cstring>
#include <iostream>
#include "GameController/GameController.h"
#include "optionparser.h"

using namespace std;
using namespace ROOT;

struct Arg: public option::Arg {
    static void printError(const char* msg1, const option::Option& opt, const char* msg2) {
        fprintf(stderr, "%s", msg1);
        fwrite(opt.name, opt.namelen, 1, stderr);
        fprintf(stderr, "%s", msg2);
        std::cin.get();
    }

    static option::ArgStatus gamerType(const option::Option& option, bool msg) {
        if (strcmp(option.arg, "CONSOLE") == 0 || strcmp(option.arg, "RANDOM") == 0 || strcmp(option.arg, "SMART") == 0)
            return option::ARG_OK;

        if (msg) printError("Option '", option, "' requires a 'CONSOLE', 'RANDOM' or 'SMART' argument\n");
        return option::ARG_ILLEGAL;
    }

    static option::ArgStatus Numeric(const option::Option& option, bool msg) {
        char* endptr = 0;
        if (option.arg != 0 && strtol(option.arg, &endptr, 10)){};
        if (endptr != option.arg && *endptr == 0)
            return option::ARG_OK;

        if (msg) printError("Option '", option, "' requires a numeric argument\n");
        return option::ARG_ILLEGAL;
    }
};

enum optionIndex {HELP, COUNT, FIRST, SECOND};
const option::Descriptor usage[] = {
        {HELP, 0, "h", "help", Arg::None, "--help, -h \tPrint usage and exit."},
        {COUNT, 0, "c", "count", Arg::Numeric, "--count, -c \tSet count of rounds."},
        {FIRST, 0, "f", "first", Arg::gamerType, "--first, -f \tSet first player type. \n(CONSOLE, RANDOM, SMART) default = RANDOM"},
        {SECOND, 0, "s", "second", Arg::gamerType, "--second, -s \tSet second player type. \n(CONSOLE, RANDOM, SMART) default = SMART"},
        { 0, 0, 0, 0, 0, 0 }
};

void initMap(std::map<std::string, GamerMode> &_map){
    _map["CONSOLE"] = CONSOLE;
    _map["RANDOM"] = RANDOM;
    _map["SMART"] = SMART;
}

int main(int argc, char* argv[]) {
    argc -= (argc > 0);
    argv += (argc > 0);
    option::Stats stats(usage, argc, argv);
    std::vector<option::Option> options(stats.options_max), buffer(stats.buffer_max);
    option::Parser parse(usage, argc, argv, &options[0], &buffer[0]);

    if (parse.error()) {
        return 1;
    }
    std::map<std::string, GamerMode> _map;
    initMap(_map);
    GameController gm;
    GamerMode gamer_1 = RANDOM,
              gamer_2 = SMART;

    if (options[HELP]) {
        option::printUsage(std::cout, usage);
        return 0;
    }

    if (options[FIRST]){
        std::string Type = options[FIRST].first()->arg;
        gamer_1 = _map[Type];
    }

    if (options[SECOND]){
        std::string Type = options[FIRST].first()->arg;
        gamer_2 = _map[Type];
    }

    int countGames = 1;
    if (options[COUNT]) {
        countGames = atoi(options[COUNT].first()->arg);
    }

    gm.run(gamer_1, gamer_2, countGames);
    return 0;
}