#include "ConsoleView.h"
#include <iostream>
#include <conio.h>
#include <windows.h>

void ConsoleView::drawMap(const Map &map) {
    system("CLS");
    std::cout << "   0 1 2 3 4 5 6 7 8 9" << std::endl;
    for (size_t i = STARTPOINT; i < COLUMNS; ++i) {
        std::cout << i << SPACE << SPACE;
        for (size_t j = STARTPOINT; j < ROWS; ++j) {
            char c = map.getCell(j, i) == EMPTY ? CLEAR_CELL : HIT_CELL;
            std::cout << c << SPACE;
        }
        std::cout << std::endl;
    }
}

void ConsoleView::drawGameState(const Map &gamer1_attack, const Map &gamer2_attack, const std::pair<size_t, size_t>& gameScore) {
    system("CLS");
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(handle, 7);
    std::cout << "                 Game score - " << gameScore.first << ':' << gameScore.second << std::endl;
    std::cout << "  gamer 1 attack map:      gamer 2 attack map:"<< std::endl;
    std::cout << "  0 1 2 3 4 5 6 7 8 9      0 1 2 3 4 5 6 7 8 9" << std::endl;

    for (size_t i = STARTPOINT; i < COLUMNS; ++i) {
        std::cout << i << SPACE;
        for (size_t j = STARTPOINT; j < DOUBLEROWS; ++j) {
            if (j < ROWS) {
                char tmp = _mapOfSymbols[gamer1_attack.getCell(j, i)];
                if (tmp == _mapOfSymbols[HIT])
                    SetConsoleTextAttribute(handle, 4);
                std::cout << tmp << SPACE;
                SetConsoleTextAttribute(handle, 7);
            }
            if (j == ROWS) std::cout << TRIPLESPACE << i <<SPACE;

            if (j > ROWS) {
                char tmp = _mapOfSymbols[gamer2_attack.getCell(j % (DOUBLEROWS - ROWS), i)];
                if (tmp == _mapOfSymbols[HIT])
                    SetConsoleTextAttribute(handle, 14);
                std::cout << tmp << SPACE;
                SetConsoleTextAttribute(handle, 7);
            }
        }
        std::cout << std::endl;
    }
}

void ConsoleView::printMessage(const std::string &message) {
    std::cout << std::endl << message << std::endl;
}

ConsoleView::ConsoleView() {
    _mapOfSymbols[EMPTY] = CLEAR_CELL;
    _mapOfSymbols[HIT] = HIT_CELL;
    _mapOfSymbols[MISS] = MISS_CELL;
}
