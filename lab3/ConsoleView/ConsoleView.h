#ifndef BATTLESHIP_CONSOLEVIEW_H
#define BATTLESHIP_CONSOLEVIEW_H

#include "../Interfaces/IGameView.h"

class ConsoleView : public IGameView{
public:
    ConsoleView();
    void drawMap(const Map &map) override;
    void drawGameState(const Map &gamer1_attack, const Map &gamer2_attack, const std::pair<size_t, size_t>& gameScore) override;
    void printMessage(const std::string &message) override;
private:
    std::map<Cell, char> _mapOfSymbols;
};

#endif //BATTLESHIP_CONSOLEVIEW_H
